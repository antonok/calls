# Occitan translation for calls.
# Copyright (C) 2021 calls's COPYRIGHT HOLDER
# This file is distributed under the same license as the calls package.
# Quentin PAGÈS <pages_quentin@hotmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: calls master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/calls/-/issues\n"
"POT-Creation-Date: 2023-01-06 17:14+0000\n"
"PO-Revision-Date: 2023-02-19 18:53+0100\n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: Occitan <totenoc@gmail.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.2.2\n"

#: data/org.gnome.Calls.desktop.in:3 data/org.gnome.Calls.metainfo.xml:6
#: src/calls-application.c:457 src/ui/call-window.ui:9 src/ui/main-window.ui:7
msgid "Calls"
msgstr "Sonadas"

#: data/org.gnome.Calls.desktop.in:4 data/org.gnome.Calls-daemon.desktop.in:4
msgid "Phone"
msgstr "Telefòn"

#: data/org.gnome.Calls.desktop.in:5
msgid "A phone dialer and call handler"
msgstr "Un telefòn e gestionari de sonadas"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Calls.desktop.in:7 data/org.gnome.Calls-daemon.desktop.in:7
msgid "Telephone;Call;Phone;Dial;Dialer;PSTN;"
msgstr "Telefòn;sonada;sonar;VoIP;PSTN;crida;apèl;"

#: data/org.gnome.Calls-daemon.desktop.in:3
msgid "Calls (daemon)"
msgstr ""

#: data/org.gnome.Calls-daemon.desktop.in:5
msgid "A phone dialer and call handler (daemon mode)"
msgstr ""

#: data/org.gnome.Calls.metainfo.xml:7
msgid "Make phone and SIP calls"
msgstr ""

#: data/org.gnome.Calls.metainfo.xml:10
msgid ""
"Calls is a simple, elegant phone dialer and call handler for GNOME. It can "
"be used with a cellular modem for plain old telephone calls as well as VoIP "
"calls using the SIP protocol."
msgstr ""

#. Translators: A screenshot description.
#: data/org.gnome.Calls.metainfo.xml:26
#, fuzzy
#| msgid "Incoming phone call"
msgid "Placing a call"
msgstr "Sonada entranta"

#. Translators: A screenshot description.
#: data/org.gnome.Calls.metainfo.xml:31
#, fuzzy
#| msgid "This call is encrypted"
msgid "The call history"
msgstr "Aquesta sonada es chifrada"

#: data/org.gnome.Calls.gschema.xml:7 data/org.gnome.Calls.gschema.xml:8
msgid "Whether calls should automatically use the default origin"
msgstr ""

#: data/org.gnome.Calls.gschema.xml:13
msgid "The country code as reported by the modem"
msgstr ""

#: data/org.gnome.Calls.gschema.xml:14
msgid "The country code is used for contact name lookup"
msgstr ""

#: data/org.gnome.Calls.gschema.xml:19
msgid "The plugins to load automatically"
msgstr ""

#: data/org.gnome.Calls.gschema.xml:20
msgid "These plugins will be automatically loaded on application startup."
msgstr ""

#: data/org.gnome.Calls.gschema.xml:25
msgid "Audio codecs to use for VoIP calls in order of preference"
msgstr ""

#: data/org.gnome.Calls.gschema.xml:26
msgid "The preferred audio codecs to use for VoIP calls (if available)"
msgstr ""

#: data/org.gnome.Calls.gschema.xml:31
msgid "Whether to allow using SDES for SRTP without TLS as the transport"
msgstr ""

#: data/org.gnome.Calls.gschema.xml:32
msgid "Set to true if you want to allow with keys exchanged in cleartext."
msgstr ""

#: src/calls-account.c:163
msgid "Default (uninitialized) state"
msgstr ""

#: src/calls-account.c:166
msgid "Initializing account…"
msgstr ""

#: src/calls-account.c:169
msgid "Uninitializing account…"
msgstr ""

#: src/calls-account.c:172
msgid "Connecting to server…"
msgstr "Connexion al servidor…"

#: src/calls-account.c:175
msgid "Account is online"
msgstr ""

#: src/calls-account.c:178
msgid "Disconnecting from server…"
msgstr ""

#: src/calls-account.c:181
msgid "Account is offline"
msgstr ""

#: src/calls-account.c:184
msgid "Account encountered an error"
msgstr ""

#: src/calls-account.c:202
msgid "No reason given"
msgstr "Sens rason"

#: src/calls-account.c:205
msgid "Initialization started"
msgstr ""

#: src/calls-account.c:208
msgid "Initialization complete"
msgstr ""

#: src/calls-account.c:211
msgid "Uninitialization started"
msgstr ""

#: src/calls-account.c:214
msgid "Uninitialization complete"
msgstr ""

#: src/calls-account.c:217
msgid "No credentials set"
msgstr ""

#: src/calls-account.c:220
msgid "Starting to connect"
msgstr "Debuta de la connexion"

#: src/calls-account.c:223
msgid "Connection timed out"
msgstr "Lo temps de connexion a expirat"

#: src/calls-account.c:226
msgid "Domain name could not be resolved"
msgstr ""

#: src/calls-account.c:229
msgid "Server did not accept username or password"
msgstr ""

#: src/calls-account.c:232
msgid "Connecting complete"
msgstr "Processús de connexion acabat"

#: src/calls-account.c:235
msgid "Starting to disconnect"
msgstr "Debuta de la desconnexion"

#: src/calls-account.c:238
msgid "Disconnecting complete"
msgstr "Processús de desconnexion acabat"

#: src/calls-account.c:241
msgid "Internal error occurred"
msgstr "Una error intèrna s’es producha"

#: src/calls-account-overview.c:177
#, c-format
msgid "Edit account: %s"
msgstr "Modificar compte : %s"

#: src/calls-account-overview.c:184
msgid "Add new account"
msgstr "Apondre un compte novèl"

#: src/calls-account-overview.c:376
msgid "Account overview"
msgstr "Apercebut del compte"

#: src/calls-application.c:327
#, c-format
msgid "Tried dialing invalid tel URI `%s'"
msgstr ""

#: src/calls-application.c:660
#, c-format
msgid "Don't know how to open `%s'"
msgstr ""

#: src/calls-application.c:717
msgid "The name of the plugin to use as a call provider"
msgstr ""

#: src/calls-application.c:718
msgid "PLUGIN"
msgstr "EMPEUTON"

#: src/calls-application.c:723
msgid "Whether to present the main window on startup"
msgstr ""

#: src/calls-application.c:729
#, fuzzy
#| msgid "Dial a number"
msgid "Dial a telephone number"
msgstr "Apelar un numèro"

#: src/calls-application.c:730
msgid "NUMBER"
msgstr "NUMÈRO"

#: src/calls-application.c:735
msgid "Enable verbose debug messages"
msgstr ""

#: src/calls-application.c:741
msgid "Print current version"
msgstr "Afichar la version actuala"

#: src/calls-best-match.c:435
msgid "Anonymous caller"
msgstr "Sonaire anonim"

#: src/calls-call-record-row.c:97
#, c-format
msgid ""
"%s\n"
"yesterday"
msgstr ""
"%s\n"
"ièr"

#: src/calls-main-window.c:123
msgid "translator-credits"
msgstr "Cédric Valmary (totenoc.eu) <cvalmary@yahoo.fr>."

#: src/calls-main-window.c:316
msgid "Can't place calls: No modem or VoIP account available"
msgstr ""

#: src/calls-main-window.c:318
msgid "Can't place calls: No plugin loaded"
msgstr ""

#: src/calls-main-window.c:353
msgid "Contacts"
msgstr "Contactes"

#: src/calls-main-window.c:363
msgid "Dial Pad"
msgstr "Clavièr"

#. Recent as in "Recent calls" (the call history)
#: src/calls-main-window.c:372
msgid "Recent"
msgstr "Recents"

#: src/calls-notifier.c:52
msgid "Missed call"
msgstr "Sonadas mancadas"

#. %s is a name here
#: src/calls-notifier.c:76
#, c-format
msgid "Missed call from <b>%s</b>"
msgstr "Sonada mancada de <b>%s</b>"

#. %s is a id here
#: src/calls-notifier.c:79
#, c-format
msgid "Missed call from %s"
msgstr "Sonada mancada de %s"

#: src/calls-notifier.c:81
msgid "Missed call from unknown caller"
msgstr "Sonada mancada d’un sonaire anonim"

#: src/calls-notifier.c:87
msgid "Call back"
msgstr "Tornar sonar"

#: src/ui/account-overview.ui:16
msgid "VoIP Accounts"
msgstr "Comptes VoIP"

#: src/ui/account-overview.ui:49
msgid "Add VoIP Accounts"
msgstr "_Apondre comptes VoIP"

#: src/ui/account-overview.ui:51
msgid ""
"You can add VoIP account here. It will allow you to place and receive VoIP "
"calls using the SIP protocol. This feature is still relatively new and not "
"yet feature complete (i.e. no encrypted media)."
msgstr ""

#: src/ui/account-overview.ui:58 src/ui/account-overview.ui:106
msgid "_Add Account"
msgstr "_Apondre un compte"

#. Translators: This is a verb, not a noun. Call the number of the currently selected row.
#: src/ui/call-record-row.ui:62
msgid "Call"
msgstr "Apèl"

#: src/ui/call-record-row.ui:102
msgid "_Delete Call"
msgstr "_Suprimir la sonada"

#. Translators: This is a phone number
#: src/ui/call-record-row.ui:107
msgid "_Copy number"
msgstr "_Copiar lo numèro"

#: src/ui/call-record-row.ui:112
msgid "_Add contact"
msgstr "_Apondre contacte"

#: src/ui/call-record-row.ui:117
msgid "_Send SMS"
msgstr "_Enviar SMS"

#: src/ui/call-selector-item.ui:38
msgid "On hold"
msgstr "En espèra"

#: src/ui/contacts-box.ui:60
msgid "No Contacts Found"
msgstr "Cap de contacte pas trobat"

#: src/ui/history-box.ui:10
msgid "No Recent Calls"
msgstr "Cap de sonada recenta"

#: src/ui/main-window.ui:105
msgid "USSD"
msgstr "USSD"

#: src/ui/main-window.ui:114
msgid "_Cancel"
msgstr "_Anullar"

#: src/ui/main-window.ui:131
msgid "_Close"
msgstr "_Tampar"

#: src/ui/main-window.ui:141
msgid "_Send"
msgstr "_Enviar"

#: src/ui/main-window.ui:214
msgid "_VoIP Accounts"
msgstr "_Comptes VoIP"

#: src/ui/main-window.ui:227
msgid "_Keyboard shortcuts"
msgstr "_Acorchis de clavièr"

#: src/ui/main-window.ui:233
msgid "_Help"
msgstr "_Ajuda"

#. "Calls" is the application name, do not translate
#: src/ui/main-window.ui:239
msgid "_About Calls"
msgstr "_A prepaus de Sonadas"

#: src/ui/new-call-box.ui:38
msgid "Enter a VoIP address"
msgstr "Picatz una adreça VoIP"

#: src/ui/new-call-box.ui:62
msgid "SIP Account"
msgstr "Compte SIP"

#: src/ui/new-call-header-bar.ui:6
msgid "New Call"
msgstr "Sonada novèla"

#: src/ui/new-call-header-bar.ui:19
msgid "Back"
msgstr "Retorn"

#: plugins/provider/mm/calls-mm-call.c:73
msgid "Unknown reason"
msgstr "Rason desconeguda"

#: plugins/provider/mm/calls-mm-call.c:74
msgid "Outgoing call started"
msgstr ""

#: plugins/provider/mm/calls-mm-call.c:75
#, fuzzy
#| msgid "Incoming phone call"
msgid "New incoming call"
msgstr "Sonada entranta"

#: plugins/provider/mm/calls-mm-call.c:76
msgid "Call accepted"
msgstr "Apèl acceptat"

#: plugins/provider/mm/calls-mm-call.c:77
msgid "Call ended"
msgstr "Sonada acabada"

#: plugins/provider/mm/calls-mm-call.c:78
msgid "Call disconnected (busy or call refused)"
msgstr ""

#: plugins/provider/mm/calls-mm-call.c:79
msgid "Call disconnected (wrong id or network problem)"
msgstr ""

#: plugins/provider/mm/calls-mm-call.c:80
msgid "Call disconnected (error setting up audio channel)"
msgstr ""

#. Translators: Transfer is for active or held calls
#: plugins/provider/mm/calls-mm-call.c:82
msgid "Call transferred"
msgstr "Apèl transferit"

#. Translators: Deflecting is for incoming or waiting calls
#: plugins/provider/mm/calls-mm-call.c:84
msgid "Call deflected"
msgstr ""

#: plugins/provider/mm/calls-mm-call.c:109
#, c-format
msgid "Call disconnected (unknown reason code %i)"
msgstr ""

#: plugins/provider/mm/calls-mm-provider.c:84
msgid "ModemManager unavailable"
msgstr ""

#: plugins/provider/mm/calls-mm-provider.c:86
#: plugins/provider/ofono/calls-ofono-provider.c:96
msgid "No voice-capable modem available"
msgstr ""

#: plugins/provider/mm/calls-mm-provider.c:88
#: plugins/provider/ofono/calls-ofono-provider.c:98
msgid "Normal"
msgstr "Normala"

#: plugins/provider/mm/calls-mm-provider.c:456
#: plugins/provider/ofono/calls-ofono-provider.c:546
msgid "Initialized"
msgstr ""

#: plugins/provider/ofono/calls-ofono-provider.c:94
msgid "DBus unavailable"
msgstr ""

#: plugins/provider/sip/calls-sip-account-widget.c:668
msgid "No encryption"
msgstr "Pas de chiframent"

#. TODO Optional encryption
#: plugins/provider/sip/calls-sip-account-widget.c:675
msgid "Force encryption"
msgstr "Forçar lo chiframent"

#: plugins/provider/sip/calls-sip-call.c:123
msgid "Cryptographic key exchange unsuccessful"
msgstr ""

#: plugins/provider/sip/sip-account-widget.ui:11
msgid "Add Account"
msgstr "Apondre un compte"

#: plugins/provider/sip/sip-account-widget.ui:17
msgid "_Log In"
msgstr "_Se connectar"

#: plugins/provider/sip/sip-account-widget.ui:42
msgid "Manage Account"
msgstr "Gestion de compte"

#: plugins/provider/sip/sip-account-widget.ui:47
msgid "_Apply"
msgstr "_Aplicar"

#: plugins/provider/sip/sip-account-widget.ui:61
msgid "_Delete"
msgstr "Su_primir lo camin"

#: plugins/provider/sip/sip-account-widget.ui:91
msgid "Server"
msgstr "Servidor"

#: plugins/provider/sip/sip-account-widget.ui:109
msgid "Display Name"
msgstr "Nom d'afichatge"

#: plugins/provider/sip/sip-account-widget.ui:110
msgid "Optional"
msgstr "Opcional"

#: plugins/provider/sip/sip-account-widget.ui:128
msgid "User ID"
msgstr "ID de l’utilizaire"

#: plugins/provider/sip/sip-account-widget.ui:141
msgid "Password"
msgstr "Senhal"

#: plugins/provider/sip/sip-account-widget.ui:166
msgid "Port"
msgstr "Pòrt :"

#: plugins/provider/sip/sip-account-widget.ui:182
msgid "Transport"
msgstr "Transpòrt"

#: plugins/provider/sip/sip-account-widget.ui:189
msgid "Media Encryption"
msgstr ""

#: plugins/provider/sip/sip-account-widget.ui:201
msgid "Use for Phone Calls"
msgstr ""

#: plugins/provider/sip/sip-account-widget.ui:214
msgid "Automatically Connect"
msgstr ""

#~ msgid "Calling…"
#~ msgstr "Sonada…"

#~ msgid "Mute"
#~ msgstr "Amudir"

#~ msgid "Speaker"
#~ msgstr "Nautparlaire"

#~ msgid "Add call"
#~ msgstr "Apondre una sonada"

#~ msgid "Hold"
#~ msgstr "Metre en espèra"

#~ msgid "Hang up"
#~ msgstr "Penjar"

#~ msgid "Answer"
#~ msgstr "Respondre"

#~ msgid "Hide the dial pad"
#~ msgstr "Amagar lo clavièr"

#~ msgid "+441234567890"
#~ msgstr "+441234567890"

#~ msgid "This call is not encrypted"
#~ msgstr "Aquesta sonada es pas chifrada"

#~ msgid "Menu"
#~ msgstr "Menú"

#~ msgid "About Calls"
#~ msgstr "A prepaus de Sonadas"

#~ msgid "No modem found"
#~ msgstr "Cap de modem pas trobat"

#~ msgid "Enter a number"
#~ msgstr "Picatz un numèro"

#~ msgid "Dial"
#~ msgstr "Sonar"
